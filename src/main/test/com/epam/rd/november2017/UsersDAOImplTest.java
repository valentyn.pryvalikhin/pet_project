package com.epam.rd.november2017;

import com.epam.rd.november2017.controller.DBConnectionUtil;
import com.epam.rd.november2017.database.dao.UsersDAOImpl;
import com.epam.rd.november2017.database.entity.Users;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UsersDAOImplTest {

    static Connection connection = DBConnectionUtil.getTestConnection();
    UsersDAOImpl udi = new UsersDAOImpl(connection);

    @BeforeClass
    public static void initialisation() {

        Connection connection = DBConnectionUtil.getTestConnection();

        try {
            connection.createStatement().executeUpdate("CREATE TABLE users (" +
                    "ID INT(11) NOT NULL AUTO_INCREMENT, " +
                    "NAME VARCHAR(100) NOT NULL, " +
                    "SURNAME VARCHAR(100) NOT NULL, " +
                    "E_MAIL VARCHAR(100) NOT NULL, " +
                    "LOGIN VARCHAR(100) NOT NULL, " +
                    "PASSWORD VARCHAR(100) NOT NULL, " +
                    "BLOCKED TINYINT(4) NOT NULL DEFAULT '0', " +
                    "ADMIN TINYINT(4) NOT NULL DEFAULT '0', " +
                    "PRIMARY KEY (ID))");

            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users (name," +
                    " surname, e_mail, login, password) VALUES " +
                    "('Gwyneth', 'Paltrow', 'paltrow@mail.com', 'paltrow', '123456a')," +
                    "('Rachel','McAdams', 'adams@ukr.net', 'adams', 'qwerty')," +
                    "('Anne','Hathaway', 'ann@mail.com', 'ann', '123')," +
                    "('Kate','Beckinsale', 'katya@ukr.net', 'katya', 'sale')," +
                    "('Olivia','Wilde', 'olva@mail.com', 'olva', '654321')," +
                    "('Gal','Gadot', 'gal@ukr.net', 'gal', 'qwerty123')");
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void tearDown() {
        try {
            if(connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            System.out.println("Failed to close connection!");
        }
    }

    @Test
    public void testAdd() throws Exception {
        Users addedUser = new Users();
        addedUser.setId(7);
        addedUser.setName("Elizabeth");
        addedUser.setSurname("Olsen");
        addedUser.setEmail("olsen@yahoo.com");
        addedUser.setLogin("olsen");
        addedUser.setPassword("qazwsx1");
        udi.add(addedUser);
        assertEquals(addedUser, udi.getByLogin("olsen"));

        Users addedUser1 = new Users();
        addedUser1.setId(8);
        addedUser1.setName("Keira");
        addedUser1.setSurname("Knightly");
        addedUser1.setEmail("keira@mail.com");
        addedUser1.setLogin("keira");
        addedUser1.setPassword("gfdssq67");
        udi.add(addedUser1);
        assertEquals(addedUser1, udi.getByLogin("keira"));
    }

    @Test
    public void testGetByLogin() {
        Users user1 = new Users();
        user1.setId(1);
        user1.setName("Gwyneth");
        user1.setSurname("Paltrow");
        user1.setEmail("paltrow@mail.com");
        user1.setLogin("paltrow");
        user1.setPassword("123456a");
        assertEquals(user1, udi.getByLogin("paltrow"));

        Users user2 = new Users();
        user2.setId(5);
        user2.setName("Olivia");
        user2.setSurname("Wilde");
        user2.setEmail("olva@mail.com");
        user2.setLogin("olva");
        user2.setPassword("654321");
        assertEquals(user2, udi.getByLogin("olva"));
    }

    @Test
    public void testUpdate() {
        Users user = udi.getByLogin("olva");
        user.setName("Galya");
        udi.update(user);
        assertEquals(user, udi.getByLogin("olva"));
    }


    @Test
    public void testDelete() {
        Users user = udi.getByLogin("ann");
        udi.delete(user);
        assertNull(udi.getByLogin("ann"));
    }
}