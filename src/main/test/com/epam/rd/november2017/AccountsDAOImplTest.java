package com.epam.rd.november2017;

import com.epam.rd.november2017.controller.DBConnectionUtil;
import com.epam.rd.november2017.database.dao.AccountsDAOImpl;
import com.epam.rd.november2017.database.entity.Accounts;
import com.epam.rd.november2017.database.entity.Users;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AccountsDAOImplTest {
    static Connection connection = DBConnectionUtil.getTestConnection();
    AccountsDAOImpl adi = new AccountsDAOImpl(connection);

    @BeforeClass
    public static void initialisation() {

        Connection connection = DBConnectionUtil.getTestConnection();

        try {
            connection.createStatement().executeUpdate("CREATE TABLE accounts (" +
                    "CARD_NUM BIGINT(16) NOT NULL, " +
                    "NAME VARCHAR(50) NULL, " +
                    "AMOUNT DOUBLE NOT NULL, " +
                    "USER INT(11) NOT NULL, " +
                    "BLOCKED  TINYINT(4) NOT NULL DEFAULT '0', " +
                    "STATE TINYINT(4) NOT NULL DEFAULT '0', " +
                    "PRIMARY KEY (CARD_NUM))");

            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO accounts (card_num," +
                    " name, amount, user) VALUES " +
                    "(1234567890123456, 'acc1', 200, 1)," +
                    "(1111222233334444, 'acc2', 0, 2)," +
                    "(5555777733336666, 'acc3', 150, 3)," +
                    "(3333666688886666,'acc4', 15, 1)," +
                    "(9876543210987654, 'acc5', 300, 4)," +
                    "(5555444422226666, 'acc6', 1000, 5)");
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void tearDown() {
        try {
            if(connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            System.out.println("Failed to close connection!");
        }
    }
    @Test
    public void testAdd() throws Exception {
        Accounts addedAccount = new Accounts();
        addedAccount.setCardNumber(783253);
        addedAccount.setName("My card");
        addedAccount.setAmount(100.60);
        addedAccount.setUser(2);
        adi.add(addedAccount);
        Accounts acc = adi.getByCardNumber((long) 783253);
        assertEquals(addedAccount, acc);
    }

    @Test
    public void testGetByCardNumber() {
        Accounts account = new Accounts();
        account.setCardNumber(1234567890123456L);
        account.setName("acc1");
        account.setAmount(200);
        account.setUser(1);
        assertEquals(account, adi.getByCardNumber(1234567890123456L));
    }

    @Test
    public void testUpdate() {
        Accounts account = adi.getByCardNumber(3333666688886666L);
        account.setName("My super card");
        adi.update(account);
        assertEquals(account, adi.getByCardNumber(3333666688886666L));
    }


    @Test
    public void testDelete() {
        Accounts account = adi.getByCardNumber(9876543210987654L);
        adi.delete(account);
        assertNull(adi.getByCardNumber(9876543210987654L));
    }
}
