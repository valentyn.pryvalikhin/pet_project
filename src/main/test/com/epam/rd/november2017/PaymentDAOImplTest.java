package com.epam.rd.november2017;

import com.epam.rd.november2017.controller.DBConnectionUtil;
import com.epam.rd.november2017.database.dao.PaymentsDAOImpl;
import com.epam.rd.november2017.database.entity.Payments;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class PaymentDAOImplTest {
    static Connection connection = DBConnectionUtil.getTestConnection();
    PaymentsDAOImpl pdi = new PaymentsDAOImpl(connection);

    @BeforeClass
    public static void initialisation() {

        Connection connection = DBConnectionUtil.getTestConnection();

        try {
            connection.createStatement().executeUpdate("CREATE TABLE payments (" +
                    "ID INT(11) NOT NULL AUTO_INCREMENT, " +
                    "AMOUNT DOUBLE NOT NULL, " +
                    "BEN_ACCOUNT BIGINT(16) NOT NULL, " +
                    "STATUS TINYINT(4) NOT NULL DEFAULT '0', " +
                    "DATE TIMESTAMP NOT NULL DEFAULT '', " +
                    "ACCOUNT BIGINT(20) NOT NULL,, " +
                    "USER_ID INT(11) NOT NULL, " +
                    "PRIMARY KEY (ID))");

            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO payments (amount," +
                    " ben_account, account, date, user_id) VALUES " +
                    "(10, 1234567890123456, 1111222233334444 '2018-01-11', 1)," +
                    "(200.50, 1111222233334444, 3333666688886666, '2018-02-03', 2)");

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void tearDown() {
        try {
            if(connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            System.out.println("Failed to close connection!");
        }
    }
}
