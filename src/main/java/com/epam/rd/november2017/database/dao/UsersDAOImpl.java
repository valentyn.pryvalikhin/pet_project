package com.epam.rd.november2017.database.dao;

import com.epam.rd.november2017.database.entity.Users;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class UsersDAOImpl implements UsersDAO {
    private static String ID = "id";
    private static String NAME = "name";
    private static String SURNAME = "surname";
    private static String EMAIL = "e_mail";
    private static String LOGIN = "login";
    private static String PASSWORD = "password";
    private static String ADMIN = "admin";
    private static String BLOCKED = "blocked";

    private Connection connection;

    public UsersDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void add(Users user) {
        String sql = "INSERT INTO users (name, surname, e_mail, login, password, admin, blocked) VALUES (?, ?, ?, ? ,?, ?, ?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            //preparedStatement.setInt(1, user.getId());
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getLogin());
            preparedStatement.setString(5, user.getPassword());
            if (user.getAdmin()) {
                preparedStatement.setInt(6, 1);
            } else {
                preparedStatement.setInt(6, 0);
            }
            if (user.getBlocked()) {
                preparedStatement.setInt(7, 1);
            } else {
                preparedStatement.setInt(7, 0);
            }
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method add()");
        }
    }

    @Override
    public Users getById(Integer id) {
        String sql = "SELECT * FROM users WHERE id = " + id;
        Users user = null;

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();
            user = proccesUser(resultSet);

        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method getById()");
        }

        return user;
    }

    @Override
    public Users getByLogin(String login) {
        String sql = "SELECT * FROM users WHERE login = ".concat("'" + login + "'");
        Users user = null;

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();
            user = proccesUser(resultSet);

        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method getByLogin()");
        }

        return user;
    }

    @Override
    public List<Users> getAll() {
        String sql = "SELECT * FROM users";
        List<Users> users = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            ResultSet resultSet = preparedStatement.executeQuery();
            users = proccesUsers(resultSet);

        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method getAll()");
        }
        return users;
    }

    @Override
    public void update(Users user) {
        String sql = "UPDATE users SET name = ?, surname = ?, e_mail = ?, login = ?, password = ?, admin = ?, blocked = ? WHERE login = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getLogin());
            preparedStatement.setString(5, user.getPassword());
            if (user.getAdmin()) {
                preparedStatement.setInt(6, 1);
            } else {
                preparedStatement.setInt(6, 0);
            }
            if (user.getBlocked()) {
                preparedStatement.setInt(7, 1);
            } else {
                preparedStatement.setInt(7, 0);
            }
            preparedStatement.setString(8, user.getLogin());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method update()");
        }
    }

    @Override
    public void delete(Users user) {
        String sql = "DELETE FROM users WHERE login = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method delete()");
        }
    }

    private List<Users> proccesUsers(ResultSet resultSet) throws SQLException {
        List<Users> users = new ArrayList<>();
        while (resultSet.next()) {
            users.add(proccesUser(resultSet));
        }
        return users;
    }

    private Users proccesUser(ResultSet resultSet) throws SQLException {
        Users user = new Users();

        user.setId(resultSet.getInt(ID));
        user.setName(resultSet.getString(NAME));
        user.setSurname(resultSet.getString(SURNAME));
        user.setEmail(resultSet.getString(EMAIL));
        user.setLogin(resultSet.getString(LOGIN));
        user.setPassword(resultSet.getString(PASSWORD));

        if (resultSet.getInt(ADMIN) == 0) {
            user.setAdmin(false);
        } else {
            user.setAdmin(true);
        }
        if (resultSet.getInt(BLOCKED) == 0) {
            user.setBlocked(false);
        } else {
            user.setBlocked(true);
        }
        return user;
    }
}