package com.epam.rd.november2017.database.entity;

import java.util.Objects;

public class Accounts {
    private long cardNumber;
    private String name;
    private double amount;
    private int user;
    private byte state;

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public byte getState() { return state; }

    public void setState(byte state) { this.state = state; }

    @Override
    public String toString() {
        return "Accounts{" +
                "cardNumber=" + cardNumber +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", user=" + user +
                ", state=" + state +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Accounts accounts = (Accounts) o;
        return cardNumber == accounts.cardNumber &&
                Double.compare(accounts.amount, amount) == 0 &&
                user == accounts.user &&
                state == accounts.state &&
                Objects.equals(name, accounts.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(cardNumber, name, amount, user, state);
    }
}
