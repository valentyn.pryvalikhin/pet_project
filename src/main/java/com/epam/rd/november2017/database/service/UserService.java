package com.epam.rd.november2017.database.service;

import com.epam.rd.november2017.database.dao.UsersDAOImpl;
import com.epam.rd.november2017.database.entity.Users;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class UserService {

    public static Users getVerifiedUser(UsersDAOImpl udi, String userLogin, String userPassword) {

        Users user = udi.getByLogin(userLogin);

        if (user != null) {
            if (user.getPassword().equals(userPassword)) {
                return user;
            } else {
                return null;
            }
        }

        return user;
    }
}
