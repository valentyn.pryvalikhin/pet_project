package com.epam.rd.november2017.database.dao;

import com.epam.rd.november2017.database.entity.Accounts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccountsDAOImpl implements AccountsDAO {
    private static String CARD_NUM = "card_num";
    private static String NAME = "name";
    private static String AMOUNT = "amount";
    private static String USER = "user";
    private static String STATE = "state";

    private Connection connection;

    public AccountsDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void add(Accounts account) {
        String sql = "INSERT INTO accounts (card_num, name, amount, user) VALUES (?, ?, ?, ?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, account.getCardNumber());
            preparedStatement.setString(2, account.getName());
            preparedStatement.setDouble(3, account.getAmount());
            preparedStatement.setInt(4, account.getUser());
            //preparedStatement.setByte(5, account.getState());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method add()");
        }
    }

    @Override
    public Accounts getByCardNumber(Long cardNumber) {
        String sql = "SELECT * FROM accounts WHERE card_num = ?";
        Accounts account = null;

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, cardNumber);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            account = proccesAccount(resultSet);
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method getById()");
            e.printStackTrace();
        }
        return account;
    }

    @Override
    public List<Accounts> getAll() {
        String sql = "SELECT * FROM accounts";
        List<Accounts> accounts = new ArrayList<>();


        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            accounts = proccesAccounts(resultSet);
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method getAll()");
        }
        return accounts;
    }

    @Override
    public void update(Accounts account) {
        String sql = "UPDATE accounts SET name = ?, amount = ?, user = ?, state = ? WHERE card_num = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, account.getName());
            preparedStatement.setDouble(2, account.getAmount());
            preparedStatement.setInt(3, account.getUser());
            preparedStatement.setByte(4, account.getState());

            preparedStatement.setLong(5, account.getCardNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method update()");
        }
    }

    @Override
    public void delete(Accounts account) {
        String sql = "DELETE FROM accounts WHERE card_num = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, account.getCardNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method delete()");
        }
    }

    @Override
    public List<Accounts> getByUserID(int id) {
        String sql = "SELECT *  FROM ACCOUNTS WHERE user =" + id;
        List<Accounts> accounts = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            accounts = proccesAccounts(resultSet);
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method getByUserID()");
        }
        return accounts;
    }

    private List<Accounts> proccesAccounts(ResultSet resultSet) throws SQLException {
        List<Accounts> accounts = new ArrayList<>();
        while (resultSet.next()) {
            accounts.add(proccesAccount(resultSet));
        }
        return accounts;
    }

    private Accounts proccesAccount(ResultSet resultSet) throws SQLException {
        Accounts account = new Accounts();

        account.setCardNumber(resultSet.getLong(CARD_NUM));
        account.setName(resultSet.getString(NAME));
        account.setAmount(resultSet.getDouble(AMOUNT));
        account.setUser(resultSet.getInt(USER));
        account.setState(resultSet.getByte(STATE));

        return account;
    }
}
