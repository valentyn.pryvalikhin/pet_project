package com.epam.rd.november2017.database.service;

import com.epam.rd.november2017.controller.DBConnectionUtil;
import com.epam.rd.november2017.database.dao.UsersDAOImpl;
import com.epam.rd.november2017.database.entity.Users;

import java.util.regex.Pattern;

public class ExistenceCheck {
    UsersDAOImpl udi;

    public ExistenceCheck(UsersDAOImpl udi) {
        this.udi = udi;
    }

    public boolean checkUserExistence(String userLogin) {
        Users user = udi.getByLogin(userLogin);
        if (user != null) {
            return true;
        } else {
            return false;
        }
    }
}
