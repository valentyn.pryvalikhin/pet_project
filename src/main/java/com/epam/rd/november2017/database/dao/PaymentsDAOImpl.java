package com.epam.rd.november2017.database.dao;

import com.epam.rd.november2017.database.entity.Payments;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PaymentsDAOImpl implements PaymentsDAO {
    private static String ID = "id";
    private static String AMOUNT = "amount";
    private static String BENACCOUNT = "ben_account";
    private static String STATUS = "status";
    private static String ACCOUNT = "account";
    private static String DATE = "date";
    private static String USER_ID = "user_id";

    private Connection connection;

    public PaymentsDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void add(Payments payment) {
        String sql = "INSERT INTO payments (amount, ben_account, status, account, date, user_id) VALUES (?, ?, ?, ?, ?, ?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setDouble(1, payment.getAmount());
            preparedStatement.setLong(2, payment.getBenAccount());
            preparedStatement.setByte(3, payment.getStatus());
            preparedStatement.setLong(4, payment.getAccount());
            preparedStatement.setDate(5, new java.sql.Date(payment.getDate().getTime()));
            preparedStatement.setInt(6, payment.getUserID());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method add()");
        }
    }

    @Override
    public Payments getById(int id) {
        String sql = "SELECT * FROM payments WHERE id = ?";
        Payments payment = null;

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            payment = proccesPayment(resultSet);
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method getById()");
        }
        return payment;
    }

    @Override
    public List<Payments> getAll() {
        String sql = "SELECT * FROM payments";
        List<Payments> payments = new ArrayList<>();

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            payments = proccesPayments(resultSet);
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method getAll()");
        }
        return payments;
    }

    @Override
    public void update(Payments payment) {
        String sql = "UPDATE payments SET amount = ?, ben_account = ?, status = ?, account = ?, date = ?, user_id = ? WHERE id = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setDouble(1, payment.getAmount());
            preparedStatement.setLong(2, payment.getBenAccount());
            preparedStatement.setByte(3, payment.getStatus());
            preparedStatement.setLong(4, payment.getAccount());
            preparedStatement.setDate(5, new java.sql.Date(payment.getDate().getTime()));
            preparedStatement.setInt(6, payment.getUserID());
            preparedStatement.setInt(7, payment.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method update()");
        }
    }

    @Override
    public void delete(Payments payment) {
        String sql = "DELETE FROM payments WHERE id = ?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, payment.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method delete()");
        }
    }

    @Override
    public List<Payments> getByUserID(int id) {
        String sql = "SELECT * FROM PAYMENTS WHERE USER_ID = " + id;
        List<Payments> payments = new ArrayList<>();
        Payments payment = null;

        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            payments = proccesPayments(resultSet);
        } catch (SQLException e) {
            System.out.println("Failed to complete sql query in method getByUserID()");
        }
        return payments;
    }

    private List<Payments> proccesPayments(ResultSet resultSet) throws SQLException {
        List<Payments> payments = new ArrayList<>();
        while (resultSet.next()) {
            payments.add(proccesPayment(resultSet));
        }
        return payments;
    }

    private Payments proccesPayment(ResultSet resultSet) throws SQLException {
        Payments payment = new Payments();

        payment.setId(resultSet.getInt(ID));
        payment.setAmount(resultSet.getDouble(AMOUNT));
        payment.setStatus(resultSet.getByte(STATUS));
        payment.setBenAccount(resultSet.getLong(BENACCOUNT));
        payment.setAccount(resultSet.getLong(ACCOUNT));
        payment.setDate(resultSet.getDate(DATE));
        payment.setUserID(resultSet.getInt(USER_ID));

        return payment;
    }
}
