package com.epam.rd.november2017.database.dao;

import com.epam.rd.november2017.database.entity.Users;

import java.util.List;

public interface UsersDAO {

        void add(Users user);

        Users getById(Integer id);

        Users getByLogin(String login);

        List<Users> getAll();

        void update(Users user);

        void delete(Users user);
}
