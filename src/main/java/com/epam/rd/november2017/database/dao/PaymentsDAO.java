package com.epam.rd.november2017.database.dao;

import com.epam.rd.november2017.database.entity.Payments;

import java.util.List;

public interface PaymentsDAO {
    void add(Payments payment);

    Payments getById(int id);

    List<Payments> getAll();

    public List<Payments> getByUserID(int id);

    void update(Payments payment);

    void delete(Payments payment);
}
