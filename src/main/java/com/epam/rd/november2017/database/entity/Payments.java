package com.epam.rd.november2017.database.entity;

import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

public class Payments {
    private Integer id;
    private Double amount;
    private Long account;
    private Long benAccount;
    private Byte status;
    private Integer userID;
    private Date date;

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public Double getAmount() { return amount; }

    public void setAmount(Double amount) { this.amount = amount; }

    public Long getAccount() { return account; }

    public void setAccount(Long account) { this.account = account; }

    public Long getBenAccount() { return benAccount; }

    public void setBenAccount(Long benAccount) { this.benAccount = benAccount; }

    public Byte getStatus() { return status; }

    public void setStatus(Byte status) { this.status = status; }

    public Integer getUserID() { return userID; }

    public void setUserID(Integer userID) { this.userID = userID; }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    @Override
    public String toString() {
        return "Payments{" +
                "id=" + id +
                ", amount=" + amount +
                ", account=" + account +
                ", benAccount=" + benAccount +
                ", status=" + status +
                ", userID=" + userID +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payments payments = (Payments) o;
        return Objects.equals(id, payments.id) &&
                Objects.equals(amount, payments.amount) &&
                Objects.equals(account, payments.account) &&
                Objects.equals(benAccount, payments.benAccount) &&
                Objects.equals(status, payments.status) &&
                Objects.equals(userID, payments.userID) &&
                Objects.equals(date, payments.date);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, amount, account, benAccount, status, userID, date);
    }

    public static final Comparator<Payments> COMPARE_BY_STATUS = new Comparator<Payments>() {
        @Override
        public int compare(Payments lhs, Payments rhs) {
            return lhs.getStatus() - rhs.getStatus();
        }
    };

    public static final Comparator<Payments> COMPARE_BY_DATE = new Comparator<Payments>() {
        @Override
        public int compare(Payments o1, Payments o2) {
            return o1.getDate().compareTo(o2.getDate());
        }
    };
    public static final Comparator<Payments> COMPARE_BY_ID = new Comparator<Payments>() {
        @Override
        public int compare(Payments o1, Payments o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };
}

