package com.epam.rd.november2017.database.dao;

import com.epam.rd.november2017.database.entity.Accounts;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public interface AccountsDAO {
    void add(Accounts account);

    Accounts getByCardNumber(Long cardNumber);

    List<Accounts> getAll();

    List<Accounts> getByUserID(int id);

    void update(Accounts account);

    void delete(Accounts account);

}
