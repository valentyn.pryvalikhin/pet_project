package com.epam.rd.november2017.database.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    private static Pattern namePattern = Pattern.compile("[A-ZА-Я][a-zA-Zа-яА-Я]*");
    private static Matcher nameMatcher;
    private static Pattern surnamePattern = Pattern.compile("[a-zA-zа-яА-я]+([ '-][a-zA-Zа-яА-Я]+)*");
    private static Matcher surnameMatcher;
    private static Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    private static Matcher emailMatcher;
    private static Pattern loginPattern = Pattern.compile("^[a-z0-9_-]{3,15}$");
    private static Matcher loginMatcher;
    private static Pattern moneyPattern = Pattern.compile("^\\$?(\\d{1,3},?(\\d{3},?)*\\d{3}(\\.\\d{0,2})?|\\d{1,3}(\\.\\d{0,2})?|\\.\\d{1,2}?)$");
    private static Matcher moneyMatcher;
    private static Pattern cardNumberPattern = Pattern.compile("[0-9]{16}");
    private static Matcher cardNumberMatcher;

    static public boolean validateName(final String name) {

        nameMatcher = namePattern.matcher(name);
        return nameMatcher.matches();
    }

    public static boolean validateSurname(final String surname) {

        surnameMatcher = surnamePattern.matcher(surname);
        return surnameMatcher.matches();

    }

    public static boolean validateEmail(final String email) {

        emailMatcher = emailPattern.matcher(email);
        return emailMatcher.matches();

    }

    public static boolean validateLogin(final String login) {

        loginMatcher = loginPattern.matcher(login);
        return loginMatcher.matches();

    }

    public static boolean validateMoney(final String money) {

        moneyMatcher = moneyPattern.matcher(money);
        return moneyMatcher.matches();

    }

    public static boolean validateCardNumber(final String cardNumber) {

        cardNumberMatcher = cardNumberPattern.matcher(cardNumber);
        return cardNumberMatcher.matches();

    }
}
