package com.epam.rd.november2017;

import com.epam.rd.november2017.controller.DBConnectionUtil;
import com.epam.rd.november2017.database.dao.UsersDAOImpl;
import com.epam.rd.november2017.database.entity.Users;

import java.sql.Connection;

public class Main {

    private static final String DRIVER = "org.mariadb.jdbc.Driver";
    private static final String URL = "jdbc:mariadb://localhost:3306/pet_project?useUnicode=true&characterEncoding=utf-8";
    private static final String LOGIN = "root";
    private static final String PASSWORD = "root";

    private static Connection connection;
    public static void main(String[] args) {

        Users user;

            UsersDAOImpl udi = new UsersDAOImpl(DBConnectionUtil.getConnection());

            user = udi.getById(2);
        System.out.println(user);
    }

}
