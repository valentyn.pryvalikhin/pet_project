package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.AccountsDAOImpl;
import com.epam.rd.november2017.database.dao.PaymentsDAOImpl;
import com.epam.rd.november2017.database.entity.Accounts;
import com.epam.rd.november2017.database.entity.Payments;
import com.epam.rd.november2017.database.entity.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class PaymentCreationServlet extends ConnectionServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        PaymentsDAOImpl pdi = new PaymentsDAOImpl(connection);
        Payments payment = createPayment(request);
        pdi.add(payment);

        AccountsDAOImpl adi = new AccountsDAOImpl(connection);
        List<Accounts> accounts = adi.getByUserID((Integer) session.getAttribute(StaticData.ID_ATTR));
        request.setAttribute("accounts", accounts);

        List<Payments> payments = pdi.getByUserID((Integer) session.getAttribute(StaticData.ID_ATTR));
        Collections.sort(payments, Payments.COMPARE_BY_STATUS);
        request.setAttribute("payments", payments);

        session.setAttribute(StaticData.TAB_NAME, StaticData.PAYMENTS_DATA);
        session.setAttribute(StaticData.CURRENT_PAGE, "paymentCreation");
        DBConnectionUtil.closeQuietly(connection);

        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
    }

    private Payments createPayment(HttpServletRequest request) {
        Long account = Long.valueOf(request.getParameter("selectAccount"));
        Long benAccount = Long.valueOf(request.getParameter("benCardNumber"));
        Double amount = Double.valueOf(request.getParameter("amount"));
        Integer userId = (Integer) session.getAttribute(StaticData.ID_ATTR); // looking for ID logined user
        Byte status = 0;
        if (request.getParameter("status") == null) status = 0;
        else {
            if (request.getParameter("status").equals("on")) status = 1;
        }

        Payments payment = new Payments();
        payment.setAccount(account);
        payment.setBenAccount(benAccount);
        payment.setAmount(amount);
        payment.setUserID(userId);
        payment.setStatus(status);
        payment.setDate(new Date());


        return payment;
    }
}
