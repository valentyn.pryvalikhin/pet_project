package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.UsersDAOImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GotoPersonalServlet extends LoginCheckServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        request.setAttribute("user", session.getAttribute(StaticData.VERIFIED_USER));

        session.setAttribute(StaticData.TAB_NAME, StaticData.PERSONAL_DATA);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoPersonalServlet");

        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);

        request.setAttribute("user", session.getAttribute(StaticData.VERIFIED_USER));

        session.setAttribute(StaticData.TAB_NAME, StaticData.PERSONAL_DATA);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoPersonalServlet");

        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }
}
