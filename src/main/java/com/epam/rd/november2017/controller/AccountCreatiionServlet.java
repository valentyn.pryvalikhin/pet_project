package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.AccountsDAOImpl;
import com.epam.rd.november2017.database.entity.Accounts;
import com.epam.rd.november2017.database.entity.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class AccountCreatiionServlet extends ConnectionServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        AccountsDAOImpl adi = new AccountsDAOImpl(connection);
        adi.add(createAccount(request));
        List<Accounts> accounts = adi.getByUserID((Integer) session.getAttribute(StaticData.ID_ATTR));
        request.setAttribute("accounts", accounts);

        session.setAttribute(StaticData.TAB_NAME, StaticData.ACCOUNTS_DATA);
        session.setAttribute(StaticData.CURRENT_PAGE, "accountCreatiion");
        DBConnectionUtil.closeQuietly(connection);
        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
    }

    private Accounts createAccount(HttpServletRequest request) {
        Long cardNumber = Long.valueOf(request.getParameter("cardNumber"));
        String name = request.getParameter("accountName");
        Double amount = Double.valueOf(request.getParameter("amount"));
        Integer userNum = ((Users) session.getAttribute(StaticData.VERIFIED_USER)).getId(); // looking for ID logined user

        Accounts account = new Accounts();
        account.setCardNumber(cardNumber);
        account.setName(name);
        account.setAmount(amount);
        account.setUser(userNum);

        return account;
    }
}
