package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.AccountsDAOImpl;
import com.epam.rd.november2017.database.entity.Accounts;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class GotoCreatePaymentServlet extends ConnectionServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        AccountsDAOImpl adi = new AccountsDAOImpl(connection);
        List<Accounts> accounts = adi.getByUserID((Integer) session.getAttribute(StaticData.ID_ATTR));
        List<Accounts> accountsNonBlocked = new ArrayList();      //Select unblocked accounts
        for (Accounts account : accounts) {
            if (account.getState() == 0) {
                accountsNonBlocked.add(account);
            }
        }
        request.setAttribute("accounts", accountsNonBlocked);

        session.setAttribute(StaticData.TAB_NAME, StaticData.PAYMENT_CREATION);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoCreatePayment");
        DBConnectionUtil.closeQuietly(connection);


        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);

    }

}
