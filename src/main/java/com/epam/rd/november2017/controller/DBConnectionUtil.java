package com.epam.rd.november2017.controller;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionUtil {

//    @Resource(name="jdbc/pet_project")
//    private static DataSource ds;

    private static final String DRIVER = "org.mariadb.jdbc.Driver";
    private static final String TEST_DRIVER = "org.h2.Driver";
    private static final String URL = "jdbc:mariadb://localhost:3306/pet_project?useUnicode=true&amp;characterEncoding=utf8";
    //            "useUnicode=true&characterEncoding=utf-8";
    private static final String TEST_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String LOGIN = "root";
    private static final String PASSWORD = "root";
    private static final String TEST_LOGIN = "sa";
    private static final String TEST_PASSWORD = "";


    public static Connection getConnection() {
        Connection conn = null;
//        try {
//            conn = ds.getConnection();
//        } catch (SQLException e) {
//            System.out.println("Failed to connect");
//        }

        try {
            Class.forName(DRIVER);
            conn = DriverManager.getConnection(URL, LOGIN, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Failed to connect");
        } catch (ClassNotFoundException e) {
            System.out.println("Driver isn't found!");
        }

        return conn;
    }

    public static Connection getTestConnection() {
        Connection testConn = null;

        try {
            Class.forName(TEST_DRIVER);
            testConn = DriverManager.getConnection(TEST_URL, TEST_LOGIN, TEST_PASSWORD);
        } catch (SQLException e) {
            System.out.println("Failed to connect to test DB");
        } catch (ClassNotFoundException e) {
            System.out.println("Test driver isn't found!");
        }

        return testConn;
    }

    public static void closeQuietly(Connection conn) {
        try {
            conn.close();
        } catch (Exception e) {
        }
    }

    public static void rollbackQuietly(Connection conn) {
        try {
            conn.rollback();
        } catch (Exception e) {
        }
    }


}