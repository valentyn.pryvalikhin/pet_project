package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.UsersDAOImpl;
import com.epam.rd.november2017.database.entity.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GotoUserEditServlet extends ConnectionServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
        UsersDAOImpl udi = new UsersDAOImpl(connection);

        Users userEdit = udi.getById(Integer.parseInt(request.getParameter("userForEdit")));        //Taking user for editing
        request.setAttribute("userEdit", userEdit);     //add user to request

        session.setAttribute(StaticData.TAB_NAME, StaticData.USER_EDIT);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoUserEditServlet");


        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);

        session.setAttribute(StaticData.TAB_NAME, StaticData.USER_EDIT);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoUserEditServlet");
        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }
}
