package com.epam.rd.november2017.controller.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class L10nFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpSession session = request.getSession(true);

        Locale locale = null;
        String locTag = null;

        if (req.getParameter("lang") == null) {
            if (session == null || session.getAttribute("lang") == null) {
                locTag = "en";
            } else {
                locTag = (String) session.getAttribute("lang");
            }
        } else {
            locTag = req.getParameter("lang");
        }

        if (locTag.equals("en")) {
            locale = new Locale("en", "US");
        } else if (locTag.equals("ru")) {
            locale = new Locale("ru", "RU");
        } else {
            locale = Locale.getDefault();
        }

        if (session != null) {
            session.setAttribute("lang", locale.getLanguage());
        }

        ResourceBundle locBundle = ResourceBundle.getBundle("webapp", locale);
        request.setAttribute("lang", locTag);
        request.setAttribute("locBundle", locBundle);

        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
