package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.AccountsDAOImpl;
import com.epam.rd.november2017.database.entity.Accounts;
import com.epam.rd.november2017.database.service.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class AccountInfoServlet extends ConnectionServlet {

    Accounts account;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        AccountsDAOImpl adi = new AccountsDAOImpl(connection);
        account = adi.getByCardNumber(Long.valueOf(request.getParameter("cardNumber")));
        request.setAttribute("accountForEdit", account);
        session.setAttribute(StaticData.TAB_NAME, StaticData.ACCOUNT_INFO);

        String action = request.getParameter("action");

        if (action.equals(StaticData.BLOCK)) {
            account.setState(StaticData.BLOCKED);
            adi.update(account);

        } else if (action.equals(StaticData.UNBLOCK)) {
            account.setState(StaticData.UNBLOCKING);
            adi.update(account);
            session.setAttribute(StaticData.TAB_NAME, StaticData.ACCOUNTS_DATA);

        } else if (action.equals(StaticData.DELETE)) {
            adi.delete(account);
            session.setAttribute(StaticData.TAB_NAME, StaticData.ACCOUNTS_DATA);

        } else if (action.equals(StaticData.ADD)) {
            String funds = request.getParameter("funds");
            if (Validator.validateMoney(funds) && Double.parseDouble(funds) > 0) {
                request.setAttribute("fundsAreValid", true);
                account.setAmount((account.getAmount() + Double.parseDouble(request.getParameter("funds"))));
                adi.update(account);
            } else {
                request.setAttribute("fundsAreValid", false);
            }
        }

        List<Accounts> accounts = adi.getByUserID((Integer) session.getAttribute(StaticData.ID_ATTR));
        request.setAttribute("accounts", accounts);
        session.setAttribute(StaticData.CURRENT_PAGE, "accountInfoServlet");
        DBConnectionUtil.closeQuietly(connection);



        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        session.setAttribute(StaticData.TAB_NAME, StaticData.ACCOUNT_INFO);
        session.setAttribute(StaticData.CURRENT_PAGE, "accountInfoServlet");
        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }
}
