package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.UsersDAOImpl;
import com.epam.rd.november2017.database.entity.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class GotoAdminUsersServlet extends LoginCheckServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
        UsersDAOImpl udi = new UsersDAOImpl(connection);
        List<Users> allUsers = udi.getAll();
        request.setAttribute("users", allUsers);

        session.setAttribute(StaticData.TAB_NAME, StaticData.USERS_DATA);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoAdminPanelServlet");
        DBConnectionUtil.closeQuietly(connection);

        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
        UsersDAOImpl udi = new UsersDAOImpl(connection);
        List<Users> allUsers = udi.getAll();
        request.setAttribute("users", allUsers);

        session.setAttribute(StaticData.TAB_NAME, StaticData.USERS_DATA);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoAdminPanelServlet");
        DBConnectionUtil.closeQuietly(connection);

        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);

    }
}
