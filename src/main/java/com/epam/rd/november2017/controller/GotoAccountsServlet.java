package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.AccountsDAOImpl;
import com.epam.rd.november2017.database.entity.Accounts;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class GotoAccountsServlet extends LoginCheckServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
        accInit(request);

        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
        accInit(request);

        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    private void accInit(HttpServletRequest request) {
        AccountsDAOImpl adi = new AccountsDAOImpl(connection);
        List<Accounts> accounts = adi.getByUserID((Integer) session.getAttribute(StaticData.ID_ATTR));

        if (request.getParameter("sort") != null) {
            if (request.getParameter("sort").equals("byCard"))
                accounts.sort(Comparator.comparingLong(Accounts::getCardNumber));

            if (request.getParameter("sort").equals("byName"))
                accounts.sort(Comparator.comparing(Accounts::getName));

            if (request.getParameter("sort").equals("byAmont"))
                accounts.sort(Comparator.comparingDouble(Accounts::getAmount));
        }
        request.setAttribute("accounts", accounts);

        session.setAttribute(StaticData.TAB_NAME, StaticData.ACCOUNTS_DATA);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoAccountsServlet");
        DBConnectionUtil.closeQuietly(connection);
    }
}
