package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.PaymentsDAOImpl;
import com.epam.rd.november2017.database.entity.Payments;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GotoPaymentsServlet extends LoginCheckServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        PaymentsDAOImpl pdi = new PaymentsDAOImpl(connection);
        List<Payments> payments = pdi.getByUserID((Integer) session.getAttribute(StaticData.ID_ATTR));
        request.setAttribute("payments", payments);
        Collections.sort(payments, Payments.COMPARE_BY_STATUS);

        if (request.getParameter("statusToProcess") != null) {      //change status by pressing button in list payments
            Payments payment = pdi.getById(Integer.parseInt(request.getParameter("statusToProcess")));
            payment.setStatus((byte) 1);
            pdi.update(payment);
        }
        payments = pdi.getByUserID((Integer) session.getAttribute(StaticData.ID_ATTR));
        request.setAttribute("payments", payments);
        Collections.sort(payments, Payments.COMPARE_BY_STATUS);

        session.setAttribute(StaticData.TAB_NAME, StaticData.PAYMENTS_DATA);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoPaymentsServlet");
        DBConnectionUtil.closeQuietly(connection);


        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);

        PaymentsDAOImpl pdi = new PaymentsDAOImpl(connection);
        List<Payments> payments = pdi.getByUserID((Integer) session.getAttribute(StaticData.ID_ATTR));
        request.setAttribute("payments", payments);

        Collections.sort(payments, Payments.COMPARE_BY_STATUS);

        if ((request.getParameter("sort") != null) && (request.getParameter("sort").equals("byDate"))) {
            Collections.sort(payments, Payments.COMPARE_BY_DATE);
        }
        if ((request.getParameter("sort") != null) && (request.getParameter("sort").equals("byId"))) {
            Collections.sort(payments, Payments.COMPARE_BY_ID);
        }
        if ((request.getParameter("sort") != null) && (request.getParameter("sort").equals("byStatus"))) {
            Collections.sort(payments, Payments.COMPARE_BY_STATUS);
        }

        session.setAttribute(StaticData.TAB_NAME, StaticData.PAYMENTS_DATA);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoPaymentsServlet");
        DBConnectionUtil.closeQuietly(connection);


        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }
}
