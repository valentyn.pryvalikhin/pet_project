package com.epam.rd.november2017.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.UsersDAOImpl;
import com.epam.rd.november2017.database.entity.Users;
import com.epam.rd.november2017.database.service.UserService;


public class LoginServlet extends ConnectionServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
        UsersDAOImpl udi = new UsersDAOImpl(connection);

        Users verifiedUser = UserService.getVerifiedUser(udi, request.getParameter(StaticData.LOGIN_FLD), request.getParameter(StaticData.PASSWORD_FLD));

        if (verifiedUser == null) {
            request.setAttribute("userVerificationFailed", true);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else if (verifiedUser.getBlocked()) {
            request.setAttribute("userBlocked", true);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else {
            session.setAttribute(StaticData.VERIFIED_USER, verifiedUser);
            session.setAttribute(StaticData.LOGIN_ATTR, verifiedUser.getLogin());
            session.setAttribute(StaticData.ID_ATTR, verifiedUser.getId());
            session.setAttribute(StaticData.TAB_NAME, StaticData.PERSONAL_DATA);
            request.setAttribute("user", udi.getById((Integer) session.getAttribute(StaticData.ID_ATTR)));
            session.setAttribute(StaticData.CURRENT_PAGE, "loginServlet");
            DBConnectionUtil.closeQuietly(connection);

            request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);

        if (request.getParameter("action") != null && request.getParameter("action").equals("logout")) { //Logout
            session.setAttribute(StaticData.VERIFIED_USER, null);
            session.setAttribute(StaticData.LOGIN_ATTR, null);
            session.setAttribute(StaticData.ID_ATTR, null);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }
}
