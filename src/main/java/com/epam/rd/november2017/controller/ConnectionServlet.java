package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;

public class ConnectionServlet extends HttpServlet {

    protected Connection connection;
    protected HttpSession session;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");

        connection = DBConnectionUtil.getConnection();
        request.setAttribute(StaticData.CONNECTION, connection);
        session = request.getSession();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");

        connection = DBConnectionUtil.getConnection();
        request.setAttribute(StaticData.CONNECTION, connection);
        session = request.getSession();
    }
}
