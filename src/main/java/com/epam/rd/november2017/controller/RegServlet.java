package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.UsersDAOImpl;
import com.epam.rd.november2017.database.entity.Users;
import com.epam.rd.november2017.database.service.ExistenceCheck;
import com.epam.rd.november2017.database.service.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegServlet extends ConnectionServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        UsersDAOImpl udi = new UsersDAOImpl(connection);
        ExistenceCheck existenceCheck = new ExistenceCheck(udi);

        Boolean nameIsValid = Validator.validateName(request.getParameter("name"));
        Boolean surnameIsValid = Validator.validateSurname(request.getParameter("surname"));
        Boolean emailIsValid = Validator.validateEmail(request.getParameter("email"));
        Boolean loginNotExist = !existenceCheck.checkUserExistence(request.getParameter("login"));
        Boolean loginIsValid = Validator.validateLogin(request.getParameter("login"));

        request.setAttribute("nameIsValid", nameIsValid);
        request.setAttribute("surnameIsValid", surnameIsValid);
        request.setAttribute("emailIsValid", emailIsValid);
        request.setAttribute("loginNotExist", loginNotExist);
        request.setAttribute("loginIsValid", loginIsValid);

        if (nameIsValid && surnameIsValid && emailIsValid && loginNotExist && loginIsValid) {

            Users user = createUser(request);
            udi.add(user);

            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/WEB-INF/view/registration.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);

        request.getRequestDispatcher("/WEB-INF/view/registration.jsp").forward(request, response);
        session.setAttribute(StaticData.CURRENT_PAGE, "regServlet");
    }

    private Users createUser(HttpServletRequest request) {
        Users user = new Users();
        user.setName(request.getParameter("name"));
        user.setSurname(request.getParameter("surname"));
        user.setEmail(request.getParameter("email"));
        user.setLogin(request.getParameter("login"));
        user.setPassword(request.getParameter("password"));
        return user;
    }

}
