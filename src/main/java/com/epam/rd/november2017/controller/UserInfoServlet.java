package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.UsersDAOImpl;
import com.epam.rd.november2017.database.entity.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class UserInfoServlet extends ConnectionServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        String adminUpdt = request.getParameter("adminUpdt");
        String blockUpdt = request.getParameter("blockUpdt");
        String delUser = request.getParameter("deleteUser");
        UsersDAOImpl udi = new UsersDAOImpl(connection);
        Integer userEditId = Integer.valueOf(request.getParameter("userEdit"));     //get editing user id from request
        Users userEdit = udi.getById(userEditId);

        if (adminUpdt != null && adminUpdt.equals("on")) {
            userEdit.setAdmin(true);
        }     //if checked make true
        else {
            if (adminUpdt == null) {
                userEdit.setAdmin(false);
            }       //if unchecked make false
        }
        if (blockUpdt != null && blockUpdt.equals("on")) {
            userEdit.setBlocked(true);
        }       //if checked make true
        else {
            if (blockUpdt == null) {
                userEdit.setBlocked(false);
            }     //if unchecked make false
        }
        if (delUser != null && delUser.equals("delete")) {
            udi.delete(userEdit);
        }

        udi.update(userEdit);
        List<Users> allUsers = udi.getAll();
        request.setAttribute("users", allUsers);

        session.setAttribute(StaticData.TAB_NAME, StaticData.USERS_DATA);
        session.setAttribute(StaticData.CURRENT_PAGE, "userInfo");
        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
    }
}
