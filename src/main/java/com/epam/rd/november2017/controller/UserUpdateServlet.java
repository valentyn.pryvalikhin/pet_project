package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.UsersDAOImpl;
import com.epam.rd.november2017.database.entity.Users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserUpdateServlet extends ConnectionServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        UsersDAOImpl udi = new UsersDAOImpl(connection);
        Users user = udi.getById((Integer) session.getAttribute(StaticData.ID_ATTR));

        user.setName(request.getParameter("nameFld"));
        user.setSurname(request.getParameter("surnameFld"));
        user.setEmail(request.getParameter("emailFld"));

        if (request.getParameter("passwordFld").equals(request.getParameter("passwordRFld")))
            user.setPassword(request.getParameter("passwordFld"));

        udi.update(user);
        request.setAttribute("user", udi.getById((Integer) session.getAttribute(StaticData.ID_ATTR)));

        session.setAttribute(StaticData.TAB_NAME, StaticData.PERSONAL_DATA);
        session.setAttribute(StaticData.CURRENT_PAGE, "userUpdate");
        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
    }
}
