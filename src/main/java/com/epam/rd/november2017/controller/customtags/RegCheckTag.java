package com.epam.rd.november2017.controller.customtags;

import com.epam.rd.november2017.controller.data.StaticData;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ResourceBundle;

public class RegCheckTag extends TagSupport {
    private static final long serialVersionUID = 1L;

    @Override
    public int doStartTag() throws JspException {
        try {
            HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
            HttpSession session = request.getSession();
            ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");
            if (session.getAttribute(StaticData.VERIFIED_USER) == null)
                pageContext.getOut().print("You should register");
            else
                pageContext.getOut().print(locBundle.getString("you_login_as") + " " + session.getAttribute(StaticData.LOGIN_ATTR));

        } catch (IOException ioException) {
            throw new JspException("Error: " + ioException.getMessage());
        }
        return SKIP_BODY;
    }
}