package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.AccountsDAOImpl;
import com.epam.rd.november2017.database.dao.UsersDAOImpl;
import com.epam.rd.november2017.database.entity.Accounts;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AccountsAdministrationServlet extends ConnectionServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
        AccountsDAOImpl adi = new AccountsDAOImpl(connection);

        if (request.getParameter("action").equals("unblock")) {
            Accounts acc = adi.getByCardNumber(Long.valueOf(request.getParameter("accountNumber")));
            acc.setState((byte) 0);
            adi.update(acc);
        } else {
            if (request.getParameter("action").equals("block")) {
                Accounts acc = adi.getByCardNumber(Long.valueOf(request.getParameter("accountNumber")));
                acc.setState((byte) 1);
                adi.update(acc);
            }
        }

        List<Accounts> accounts = adi.getAll();
        request.setAttribute("accounts", accounts);
        Collections.sort(accounts, new Comparator<Accounts>() {
            @Override
            public int compare(Accounts lhs, Accounts rhs) {
                return rhs.getState() - lhs.getState();
            }
        });

        session.setAttribute(StaticData.TAB_NAME, StaticData.ACCOUNTS_ADMIN);
        session.setAttribute(StaticData.CURRENT_PAGE, "accountsAdministration");

        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);

        session.setAttribute(StaticData.TAB_NAME, StaticData.ACCOUNTS_ADMIN);
        session.setAttribute(StaticData.CURRENT_PAGE, "accountsAdministration");
        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }
}
