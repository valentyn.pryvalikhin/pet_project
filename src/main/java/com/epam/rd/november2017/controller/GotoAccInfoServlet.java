package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;
import com.epam.rd.november2017.database.dao.AccountsDAOImpl;
import com.epam.rd.november2017.database.entity.Accounts;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GotoAccInfoServlet extends ConnectionServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        AccountsDAOImpl adi = new AccountsDAOImpl(connection);
        Accounts accForEdit = adi.getByCardNumber(Long.valueOf(request.getParameter("edit")));//Extract account by card num
        request.setAttribute("accountForEdit", accForEdit);

        session.setAttribute(StaticData.TAB_NAME, StaticData.ACCOUNT_INFO);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoAccInfoServlet");
        DBConnectionUtil.closeQuietly(connection);
        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);

        session.setAttribute(StaticData.TAB_NAME, StaticData.ACCOUNT_INFO);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoAccInfoServlet");
        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

}
