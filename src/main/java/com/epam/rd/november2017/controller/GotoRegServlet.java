package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GotoRegServlet extends ConnectionServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
        init(request);

        session.setAttribute(StaticData.CURRENT_PAGE, "registration");
        request.getRequestDispatcher("/WEB-INF/view/registration.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
        init(request);

        session.setAttribute(StaticData.CURRENT_PAGE, "registration");
        request.getRequestDispatcher("/WEB-INF/view/registration.jsp").forward(request, response);
    }

    private void init(HttpServletRequest request) {
        // Initialization
        request.setAttribute("nameIsValid", true);
        request.setAttribute("surnameIsValid", true);
        request.setAttribute("emailIsValid", true);
        request.setAttribute("loginNotExist", true);
        request.setAttribute("loginIsValid", true);
    }
}
