package com.epam.rd.november2017.controller;

import com.epam.rd.november2017.controller.data.StaticData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GotoCreateAccountServlet extends ConnectionServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);

        session.setAttribute(StaticData.TAB_NAME, StaticData.CREATE_ACCOUNT);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoCreateAccountServlet");

        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);

        session.setAttribute(StaticData.TAB_NAME, StaticData.CREATE_ACCOUNT);
        session.setAttribute(StaticData.CURRENT_PAGE, "gotoCreateAccountServlet");

        request.getRequestDispatcher("/WEB-INF/view/page.jsp").forward(request, response);
    }
}
