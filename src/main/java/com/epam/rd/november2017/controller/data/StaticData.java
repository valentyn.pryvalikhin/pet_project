package com.epam.rd.november2017.controller.data;
public class StaticData {

    //Request parameters
    public static String PERSONAL_DATA = "personalData";
    public static String ACCOUNTS_DATA = "accountsData";
    public static String PAYMENTS_DATA = "paymentsData";
    public static String USERS_DATA = "usersData";

    public static String CONNECTION = "connection";
    public static String SESSION = "session";

    public static String LOGIN_FLD = "loginFld";
    public static String PASSWORD_FLD = "passwordFld";

    public static String LOGIN_ATTR = "login";
    public static String ID_ATTR = "id";

    public static String TAB_NAME = "tab_name";
    public static String VERIFIED_USER = "verified_user";

    public static String USER_ID_WATCHED_BY_ADMIN = "userIDwatchedByAdmin";
    public static String USER_EDIT = "userEdit";
    public static String CREATE_ACCOUNT = "createAccount";
    public static String ACCOUNT_INFO = "accountInfo";
    public static String ACCOUNTS_ADMIN = "accountsAdmin";
    public static String PAYMENT_CREATION = "paymentCreation";
    public static String CURRENT_PAGE = "currentPage";

    // User states
    public static Byte ACTIVE = 0;
    public static Byte BLOCKED = 1;
    public static Byte UNBLOCKING = 2;

    // Account actions
    public static String BLOCK = "block";
    public static String UNBLOCK = "unblock";
    public static String DELETE = "delete";
    public static String ADD = "add";

    //Payments status
    public static Byte PREPARED = 0;
    public static Byte PROCESSING = 1;


}
