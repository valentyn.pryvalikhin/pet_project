<%@ page import="com.epam.rd.november2017.controller.data.StaticData" %>
<%@ page import="com.epam.rd.november2017.database.entity.Users" %>
<%@ page import="java.util.ResourceBundle" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title><%=locBundle.getString("payment_system")%></title>

    <script type="text/javascript">
        function gotoReg() {
            document.location.href="registration";
        }
    </script>

</head>
<jsp:include page="WEB-INF/view/header.jsp"/>
<body>
<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody>
    <tr>
        <td style="text-align:left">
            <p style="text-align:center"><%=locBundle.getString("welcome_message")%></p>
        </td>
        <td style="width:200px">
            <form name="getdata" action="loginServlet" method="post">
                <p><%=locBundle.getString("login")%></p>
                <p><input maxlength="100" name="loginFld" type="text" value="<%= request.getParameter("loginFld") == null ? "" : request.getParameter("loginFld") %>" /></p>
                <p><%=locBundle.getString("password")%></p>
                <p><input maxlength="100" name="passwordFld" type="password" /></p>
                <p><button type="submit"><%=locBundle.getString("login_action")%></button></p>
            </form>

            <p><button name="signupBtn" type="button" value="Sign up" onclick="gotoReg()"><%=locBundle.getString("reg_action")%></button></p>

            <%
                Users verifiedUser = (Users) session.getAttribute(StaticData.VERIFIED_USER);
                if (request.getAttribute("userVerificationFailed") != null) {
            %> <table border="0" cellpadding="0" cellspacing="0" id="wrongDataWarning" style="width:100%">
                <tbody>
                <tr>
                    <td style="background-color:rgb(204, 0, 0); height:50px; text-align:center; vertical-align:middle">
                        <h2><span style="font-size:14px"><span style="color:#FFFFFF"><%=locBundle.getString("warning_wrong_login_data")%></span></span></h2>
                    </td>
                </tr>
                </tbody>
            </table>
            <%
                }
                else if (request.getAttribute("userBlocked") != null) {
            %> <table border="0" cellpadding="0" cellspacing="0" id="wrongDataWarning" style="width:100%">
            <tbody>
            <tr>
                <td style="background-color:rgb(204, 0, 0); height:50px; text-align:center; vertical-align:middle">
                    <h2><span style="font-size:14px"><span style="color:#FFFFFF"><%=locBundle.getString("warning_blocked_user")%></span></span></h2>
                </td>
            </tr>
            </tbody>
        </table>
            <%} else { %> <p><%=locBundle.getString("enter_login_and_password")%></p> <% }%>

        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
