<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>

<html>
<head>
    <title><%=locBundle.getString("payment_system")%></title>
</head>
<jsp:include page="header.jsp"/>
<body>
<p>&nbsp;</p>
<form method="post" action="regServlet" id="regform"></form>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody>
    <tr>
        <td style="width: 200px; vertical-align: top;">

            <c:choose>
                <c:when test="${requestScope.nameIsValid == true}">
                    <p><%=locBundle.getString("name")%></p>
                </c:when>
                <c:otherwise>
                    <p style="color:red"><%=locBundle.getString("name_invalid")%></p>
                </c:otherwise>
            </c:choose>

            <p><input name="name" type="text" form="regform" value="<%= request.getParameter("name") == null ? "" : request.getParameter("name") %>"/></p>

               <c:choose>
                   <c:when test="${requestScope.surnameIsValid == true}">
                       <p><%=locBundle.getString("surname")%></p>
                   </c:when>
                   <c:otherwise>
                       <p style="color:red"><%=locBundle.getString("surname_invalid")%></p>
                   </c:otherwise>
               </c:choose>

            <p><input name="surname" type="text" form="regform" value="<%= request.getParameter("surname") == null ? "" : request.getParameter("surname") %>"></p>

               <c:choose>
                   <c:when test="${requestScope.emailIsValid == true}">
                       <p><%=locBundle.getString("email")%></p>
                   </c:when>
                   <c:otherwise>
                       <p style="color:red"><%=locBundle.getString("email_invalid")%></p>
                   </c:otherwise>
               </c:choose>

            <p><input name="email" type="text" form="regform" value="<%= request.getParameter("email") == null ? "" : request.getParameter("email") %>"/></p>

            <p>&nbsp;</p>
        </td>
        <td style="vertical-align: top;">
            <c:choose>
                <c:when test="${!requestScope.loginIsValid}">
                    <p style="color:red"><%=locBundle.getString("login_invalid")%></p>
                </c:when>
                <c:when test="${requestScope.loginNotExist == true}">
                    <p><%=locBundle.getString("login")%></p>
                </c:when>
                <c:otherwise>
                    <p style="color:red"><%=locBundle.getString("login_exist")%></p>
                </c:otherwise>
            </c:choose>
            <p><input name="login" type="text" form="regform" value="<%= request.getParameter("login") == null ? "" : request.getParameter("login") %>"/></p>

            <p><%=locBundle.getString("password")%></p>

            <p><input name="password" type="password" form="regform"/></p>

            <p><input name="confirm" type="submit" form="regform"/></p>
        </td>
    </tr>
    </tbody>
</table>

<p>&nbsp;
<p>&nbsp;</p>
</p>

</body>
</html>
