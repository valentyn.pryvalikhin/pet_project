<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>

<html>
<body>

        <p><%=locBundle.getString("login")%></p>

        <p><c:out value="${userEdit.login}"/></p>

        <p><%=locBundle.getString("name")%></p>

        <p><c:out value="${userEdit.name}"/></p>

        <p><%=locBundle.getString("surname")%></p>

        <p><c:out value="${userEdit.surname}"/></p>

        <p><%=locBundle.getString("email")%></p>

        <p><c:out value="${userEdit.email}"/></p>

    <form method="post" action="${pageContext.request.contextPath}/userInfo" id="updateData"></form>
    <p><input name="adminUpdt" type="checkbox" form="updateData"
      <c:if test="${userEdit.admin == true}">
      checked
      </c:if>
       /><%=locBundle.getString("user_admin_check")%>
    </p>
    <p><input name="blockUpdt" type="checkbox" form="updateData"
        <c:if test="${userEdit.blocked == true}">
        checked
        </c:if>
       /><%=locBundle.getString("block_user_action")%>
    </p>
        <button name="userEdit" type="submit" form="updateData" value="${userEdit.id}"><%=locBundle.getString("confirm_changes")%></button>
    </p>

    <form action="userInfo" id="userDelete" method="post"></form>
    <p>
        <input type="hidden" name="deleteUser" form="userDelete" value="delete">
        <button type="submit" name="userEdit" form="userDelete" value="${userEdit.id}"><%=locBundle.getString("delete_user")%></button>
    </p>

</body>
</html>
