<%@ page import="com.epam.rd.november2017.controller.data.StaticData" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody>

    <tr>
        <td>
            <p style="text-align:left"><%=locBundle.getString("user_id")%></p>
        </td>
        <td>
            <p style="text-align:left"><%=locBundle.getString("name")%></p>
        </td>
        <td>
            <p style="text-align:left"><%=locBundle.getString("surname")%></p>
        </td>
        <td>
            <p style="text-align:left"><%=locBundle.getString("email")%></p>
        </td>
        <td>
            <p style="text-align:left"><%=locBundle.getString("login")%></p>
        </td>
        <td>
            <p style="text-align:left"><%=locBundle.getString("admin")%></p>
        </td>
        <td>
            <p style="text-align:left"><%=locBundle.getString("blocked_status")%></p>
        </td>
    </tr>
    <c:forEach items="${users}" var="user">
    <form method="post" action="gotoUserEditServlet" id="userForm"></form>
    <tr>
        <td> <p style="text-align:left"><c:out value="${user.id}"></c:out></p> </td>
        <td> <p style="text-align:left"><c:out value="${user.name}"></c:out></p> </td>
        <td> <p style="text-align:left"><c:out value="${user.surname}"></c:out></p> </td>
        <td> <p style="text-align:left"><c:out value="${user.email}"></c:out></p> </td>
        <td> <p style="text-align:left"><c:out value="${user.login}"></c:out></p> </td>
        <td> <p style="text-align:left"><c:out value="${user.admin}"></c:out></p> </td>
        <td> <p style="text-align:left"><c:out value="${user.blocked}"></c:out></p> </td>
        <td> <button type="submit" name="userForEdit" form="userForm" value="${user.id}"><%=locBundle.getString("user_operations_action")%></button>  </td>
    </tr>
    </c:forEach>
    </tbody>
</table>