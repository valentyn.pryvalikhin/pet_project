<%@ page import="com.epam.rd.november2017.database.entity.Users" %>
<%@ page import="com.epam.rd.november2017.controller.data.StaticData" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>


<%Users user = (Users) session.getAttribute(StaticData.VERIFIED_USER);%>
<html>
<body>

            <form name="getdata" action="userUpdate" method="post">

                <p><%=locBundle.getString("login")%></p>

                <p>${fn:escapeXml(user.login)}</p>
                <p><%=locBundle.getString("name")%></p>

                <p><input maxlength="200" name="nameFld" type="text" pattern="[A-ZА-Я][a-zA-Zа-яА-Я]*" value="${fn:escapeXml(user.name)}"/></p>

                <p><%=locBundle.getString("surname")%></p>

                <p><input maxlength="200" name="surnameFld" type="text" pattern="[a-zA-zа-яА-я]+([ '-][a-zA-Zа-яА-Я]+)*" value="${fn:escapeXml(user.surname)}"/></p>

                <p><%=locBundle.getString("email")%></p>

                <p><input maxlength="200" name="emailFld" type="text" pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}" value="${fn:escapeXml(user.email)}"/></p>

                <p>&nbsp;</p>
                <p><b><%=locBundle.getString("change_password")%></b></p>
                <p><%=locBundle.getString("new_password")%></p>

                <p><input maxlength="200" name="passwordFld" type="password" value="${fn:escapeXml(user.password)}"/></p>
                <%--pattern="[a-zA-Z0-9-]"--%>

                <p><%=locBundle.getString("repeat_new_password")%></p>

                <p><input maxlength="200" name="passwordRFld" type="password" value="${fn:escapeXml(user.password)}"/></p>

                <p><input name="updateBtn" type="submit" value="<%=locBundle.getString("confirm_changes")%>" /></p>
            </form>
</body>
</html>