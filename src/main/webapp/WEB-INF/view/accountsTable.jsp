<%@ page import="com.epam.rd.november2017.controller.data.StaticData" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>
<script type="text/javascript">
    function gotoCreateAccount () {
        document.location.href="gotoCreateAccountServlet";
    }
</script>

<form action="gotoUserEditServlet" id="form" method="post"></form>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody>
    <tr>
        <td>
            <p style="text-align:left"><a href="gotoAccountsServlet?sort=byCard"><%=locBundle.getString("acc_card_number")%></a></p>
        </td>
        <td>
            <p style="text-align:left"><a href="gotoAccountsServlet?sort=byName"><%=locBundle.getString("acc_name")%></a></p>
        </td>
        <td>
            <p style="text-align:left"><a href="gotoAccountsServlet?sort=byAmount"><%=locBundle.getString("acc_amount")%></a></p>
        </td>
        <td>
            <p style="text-align:left"><%=locBundle.getString("acc_status")%></p>
        </td>
    </tr>

<c:forEach items="${accounts}" var="acc">
    <form method="post" action="gotoAccInfoServlet" id="operationForm"></form>
    <c:choose>
        <c:when test="${acc.state == StaticData.BLOCKED}">
        <tr style="background-color:lightgray">
            <td> <p style="text-align:left"><c:out value="${acc.cardNumber}" /></p> </td>
            <td> <p style="text-align:left"><c:out value="${acc.name}" /></p> </td>
            <td> <p style="text-align:right"><c:out value="${acc.amount}" /></p> </td>
            <td> <p style="text-align:left"><%=locBundle.getString("acc_status_blocked")%></p></td>
            <td> <button type="submit" name="edit" form="operationForm" value="${acc.cardNumber}"><%=locBundle.getString("acc_operations_action")%></button> </td>
        </tr>
        </c:when>
        <c:when test="${acc.state == StaticData.UNBLOCKING}">
        <tr style="background-color:lemonchiffon">
            <td> <p style="text-align:left"><c:out value="${acc.cardNumber}" /></p> </td>
            <td> <p style="text-align:left"><c:out value="${acc.name}" /></p> </td>
            <td> <p align="right"><c:out value="${acc.amount}" /></p> </td>
            <td> <p style="text-align:left"><%=locBundle.getString("acc_status_unblocking")%></p></td>
            <td> <button type="submit" name="edit" form="operationForm" value="${acc.cardNumber}"><%=locBundle.getString("acc_operations_action")%></button> </td>
        </tr>
        </c:when>
        <c:when test="${acc.state == StaticData.ACTIVE}">
        <tr>
            <td> <p style="text-align:left"><c:out value="${acc.cardNumber}" /></p> </td>
            <td> <p style="text-align:left"><c:out value="${acc.name}" /></p> </td>
            <td> <p><c:out value="${acc.amount}" /></p> </td>
            <td> <p style="text-align:left"><%=locBundle.getString("acc_status_active")%></p> </td>
            <td> <button type="submit" name="edit" form="operationForm" value="${acc.cardNumber}"><%=locBundle.getString("acc_operations_action")%></button> </td>
        </tr>
        </c:when>
    </c:choose>
</c:forEach>
    </tbody>
</table>
<input name="personalDataBtn" type="button" value="<%=locBundle.getString("acc_add_action")%>" onclick="gotoCreateAccount()"/>