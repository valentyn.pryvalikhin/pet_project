<%@ page import="java.util.ResourceBundle" %>
<%@ page import="com.epam.rd.november2017.controller.data.StaticData" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>

<%
    String currentLang = (String) request.getAttribute("lang");
%>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody>
    <tr>
        <td style="background-color:lightgreen; height:75px; text-align:center; width:200px">
            <h1><%=locBundle.getString("bank")%></h1>
        </td>
        <td valign="top" align="right">
            <form

          <%if (session.getAttribute(StaticData.VERIFIED_USER) == null) {%> action="/index.jsp"
            <%} else {%>                              action="/<%=session.getAttribute(StaticData.CURRENT_PAGE)%>"
            <%}%>
            method="post">
                <select name="lang">
                    <option value="en" <%= currentLang.equals("en") ? "selected" : ""%>><%=locBundle.getString("english")%></option>
                    <option value="ru" <%= currentLang.equals("ru") ? "selected" : ""%>><%=locBundle.getString("russian")%></option>
                </select>
                <button type="submit">>></button>
            </form>
        </td>
        <td valign="top" align="right" style="width:90px">
            <form action="/loginServlet">
                <button type="submit" name="action" value="logout"><%=locBundle.getString("logout")%></button>
            </form>
        </td>
    </tr>
    </tbody>
</table>

