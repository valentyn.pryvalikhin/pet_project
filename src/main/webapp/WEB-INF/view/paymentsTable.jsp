<%@ page import="java.util.List" %>
<%@ page import="com.epam.rd.november2017.controller.data.StaticData" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody>
    <tr>
        <td>
            <p style="text-align:left"><a href="gotoPaymentsServlet?sort=byId"><%=locBundle.getString("pay_id")%></a></p>
        </td>
        <td>
            <p style="text-align:left"><%=locBundle.getString("account")%></p>
        </td>
        <td>
            <p style="text-align:left"><%=locBundle.getString("pay_beneficiary_card")%></p>
        </td>
        <td>
            <p style="text-align:left"><%=locBundle.getString("pay_amount")%></p>
        </td>
        <td>
            <p style="text-align:left"><a href="gotoPaymentsServlet?sort=byDate"><%=locBundle.getString("pay_date")%></a></p>
        </td>
        <td>
            <p style="text-align:left"><a href="gotoPaymentsServlet?sort=byStatus"><%=locBundle.getString("acc_status")%></a></p>
        </td>
    </tr>
    <c:forEach items="${payments}" var="payment">
        <form method="post" action="gotoPaymentsServlet" id="operationForm"></form>
        <c:choose>
            <c:when test="${payment.status == StaticData.PREPARED}">
                <tr style="background-color:lightgray">
                    <td> <p style="text-align:left"><c:out value="${payment.id}"/></p> </td>
                    <td> <p style="text-align:left"><c:out value="${payment.account}"/></p> </td>
                    <td> <p style="text-align:left"><c:out value="${payment.benAccount}"/></p> </td>
                    <td> <p style="text-align:left"><c:out value="${payment.amount}"/></p> </td>
                    <td> <p style="text-align:left"><c:out value="${payment.date}"/></p> </td>
                    <td> <button type="submit" name="statusToProcess" form="operationForm" value="${payment.id}"><%=locBundle.getString("pay_to_process")%></button> </td>
                </tr>
            </c:when>
            <c:when test="${payment.status == StaticData.PROCESSING}">
                <tr style="background-color:#ffffff">
                    <td> <p style="text-align:left"><c:out value="${payment.id}"/></p> </td>
                    <td> <p style="text-align:left"><c:out value="${payment.account}"/></p> </td>
                    <td> <p style="text-align:left"><c:out value="${payment.benAccount}"/></p> </td>
                    <td> <p style="text-align:left"><c:out value="${payment.amount}"/></p> </td>
                    <td> <p style="text-align:left"><c:out value="${payment.date}"/></p> </td>
                    <td> <p><%=locBundle.getString("pay_processing")%></p> </td>
                </tr>
            </c:when>
        </c:choose>
    </c:forEach>
    </tbody>
</table>
<form action="gotoCreatePayment" method="post" id="createForm"></form>
<button type="submit" form="createForm" ><%=locBundle.getString("create_payment_action")%></button>