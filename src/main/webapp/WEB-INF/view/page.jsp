<%@ page import="com.epam.rd.november2017.database.entity.Users" %>
<%@ page import="com.epam.rd.november2017.controller.data.StaticData" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/regCheck.tld" prefix="z" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title><%=locBundle.getString("payment_system")%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Title</title>
    <script type="text/javascript">
        function gotoPersonalData() {
            document.location.href="gotoPersonalServlet";
        }
        function gotoAccounts() {
            document.location.href="gotoAccountsServlet";
        }
        function gotoPayments() {
            document.location.href="gotoPaymentsServlet";
        }
        function gotoUsers() {
            document.location.href="gotoAdminPanelServlet";
        }
        function gotoAdmAccounts() {
            document.location.href="gotoAdminAcc";
        }
    </script>
</head>
<body>
<jsp:include page="header.jsp"/>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody>
    <tr>
        <td style="text-align: right;">
            <button name="tabToGo"  value="personalData" onclick="gotoPersonalData ()"><%=locBundle.getString("personal_data")%></button>
            <button name="tabToGo"  value="accounts" onclick="gotoAccounts()"><%=locBundle.getString("accounts")%></button>
            <button name="tabToGo" value="payments" onclick="gotoPayments()"><%=locBundle.getString("payments")%></button>
            <c:if test="${sessionScope.verified_user.getAdmin()}">
            <button style="color:crimson" name="tabToGo" value="users" onclick="gotoUsers()"><%=locBundle.getString("users")%></button>
            <button style="color: crimson" name="tabToGo" value="admAccounts" onclick="gotoAdmAccounts()"><%=locBundle.getString("user_accounts")%></button>
            </c:if>
        </td>
    </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<%
    String tabName = (String) session.getAttribute(StaticData.TAB_NAME);

    if (tabName == StaticData.PERSONAL_DATA) {
%>
<p> <%=locBundle.getString("personal_data")%> </p>
<jsp:include page="personalInfo.jsp"/>

<%
    } else if (tabName == StaticData.ACCOUNTS_DATA) {
%>
<p> <%=locBundle.getString("accounts")%> </p>
<jsp:include page="accountsTable.jsp"/>
<%
    } else if (tabName == StaticData.PAYMENTS_DATA) {
%>
<p> <%=locBundle.getString("payments")%> </p>
<jsp:include page="paymentsTable.jsp"/>
<%
    } else if (tabName == StaticData.USERS_DATA) {
%>
<p> <%=locBundle.getString("users")%> </p>
<jsp:include page="usersTable.jsp"/>
<%
    } else if (tabName == StaticData.USER_EDIT) {
%>
<p> <%=locBundle.getString("user_edit")%> </p>
<jsp:include page="userEdit.jsp"/>
<%
    } else if (tabName == StaticData.ACCOUNT_INFO) {
%>
<p> <%=locBundle.getString("accounts_info")%> </p>
<jsp:include page="accountInfo.jsp"/>
<%
    } else if (tabName == StaticData.CREATE_ACCOUNT) {
%>
<p> <%=locBundle.getString("create_account")%> </p>
<jsp:include page="accountCreation.jsp"/>
<%
} else if (tabName == StaticData.PAYMENT_CREATION) {
%>
<p> <%=locBundle.getString("create_payment")%> </p>
<jsp:include page="paymentCreation.jsp"/>
<%
} else if (tabName == StaticData.ACCOUNTS_ADMIN) {
%>
<p> <%=locBundle.getString("admin_accounts")%> </p>
<jsp:include page="accountsAdmTable.jsp"/>
<%
    }
%>
<p style="text-align: center;"><z:regchek/></p>
</body>
</html>
