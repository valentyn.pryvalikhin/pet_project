<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>

<html>
<body>
<form action="/paymentCreation" id="accountData" method="post"></form>&nbsp;
    <p><%=locBundle.getString("acc_card_number")%></p>

    <p> <select name="selectAccount" form="accountData">
<c:forEach items="${accounts}" var="acc">
        <option value=${acc.cardNumber}>${acc.cardNumber} &nbsp;<%=locBundle.getString("balance")%>:${acc.amount}</option>
</c:forEach>
    </select>
    </p>

    <p><%=locBundle.getString("pay_beneficiary_card")%></p>
    <p><input name="benCardNumber" pattern="[0-9]{16}" type="text" form="accountData"></p>
    <p><%=locBundle.getString("pay_amount")%></p>
    <p><input name="amount" type="text" value="0.00" pattern="\d+(\.\d{2})?" form="accountData"></p>
    <p><input name="status" type="checkbox" form="accountData"><%=locBundle.getString("pay_to_process")%></p>
    <p><input type="submit" value="Submit" form="accountData"></p>

</body>
</html>
