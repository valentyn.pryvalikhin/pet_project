<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.epam.rd.november2017.controller.data.StaticData" %>
<%@ page import="com.epam.rd.november2017.database.entity.Accounts" %>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="com.epam.rd.november2017.database.dao.UsersDAOImpl" %>
<%@ page import="com.epam.rd.november2017.database.entity.Users" %>
<%@ page import="static com.epam.rd.november2017.controller.data.StaticData.*" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>

<%  List<Accounts> accounts = (List<Accounts>) request.getAttribute("accounts");
    UsersDAOImpl udi = new UsersDAOImpl( (Connection) request.getAttribute(CONNECTION) );
%>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
    <tbody>
    <tr>
        <td> <p style="text-align:left"><%=locBundle.getString("login")%></p> </td>
        <td> <p style="text-align:left"><%=locBundle.getString("name")%></p> </td>
        <td> <p style="text-align:left"><%=locBundle.getString("surname")%></p> </td>
        <td> <p style="text-align:left"><%=locBundle.getString("acc_card_number")%></p> </td>
        <td> <p style="text-align:left"><%=locBundle.getString("acc_name")%></p> </td>
        <td> <p style="text-align:left"><%=locBundle.getString("acc_amount")%></p> </td>
        <td> <p style="text-align:left"><%=locBundle.getString("acc_status")%></p> </td>
    </tr>

<% for (Accounts acc: accounts) {%>
        <form method="post" action="accountsAdministration" id="operationForm"></form>
        <%Users cuurentUser = udi.getById(acc.getUser());  // extract accounts user%>
        <%switch (acc.getState()) {
            case  1 :                                      //if state BLOCKED%>
                <tr style="background-color:lightgray">
                    <td> <p style="text-align:left"> <%=cuurentUser.getLogin()%> </p> </td>
                    <td> <p style="text-align:left"> <%=cuurentUser.getName()%> </p> </td>
                    <td> <p style="text-align:left"><%=cuurentUser.getSurname()%></p> </td>
                    <td> <p style="text-align:left"><%=acc.getCardNumber()%></p> </td>
                    <td> <p style="text-align:left"><%=acc.getName()%></p> </td>
                    <td> <p style="text-align:left"><%=acc.getAmount()%></p> </td>
                    <td> <p style="text-align:left"><%=locBundle.getString("acc_status_blocked")%></p></td>
                    <td><p><%=locBundle.getString("acc_user_waiting")%></p></td>
                </tr>
            <%;%>
            <%break; case  2 :%>
                <tr style="background-color:lemonchiffon">
                    <td> <p style="text-align:left"> <%=cuurentUser.getLogin()%> </p> </td>
                    <td> <p style="text-align:left"> <%=cuurentUser.getName()%> </p> </td>
                    <td> <p style="text-align:left"><%=cuurentUser.getSurname()%></p> </td>
                    <td> <p style="text-align:left"><%=acc.getCardNumber()%></p> </td>
                    <td> <p style="text-align:left"><%=acc.getName()%></p> </td>
                    <td> <p style="text-align:left"><%=acc.getAmount()%></p> </td>
                    <td> <p style="text-align:left"><%=locBundle.getString("acc_status_unblocking")%></p> </td>
                    <td> <input type="hidden" name="action" form="operationForm" value="unblock">
                        <button type="submit" name="accountNumber" form="operationForm" value=<%=acc.getCardNumber()%>>
                        <%=locBundle.getString("acc_unblock_action")%></button> </td>
                </tr>
            <%;%>
            <%break; case  0 :%>
                <tr>
                    <td> <p style="text-align:left"> <%=cuurentUser.getLogin()%> </p> </td>
                    <td> <p style="text-align:left"> <%=cuurentUser.getName()%> </p> </td>
                    <td> <p style="text-align:left"><%=cuurentUser.getSurname()%></p> </td>
                    <td> <p style="text-align:left"><%=acc.getCardNumber()%></p> </td>
                    <td> <p style="text-align:left"><%=acc.getName()%></p> </td>
                    <td> <p style="text-align:left"><%=acc.getAmount()%></p> </td>
                    <td> <p style="text-align:left"><%=locBundle.getString("acc_status_active")%></p> </td>
                    <td> <input type="hidden" name="action" form="operationForm" value="block">
                        <button type="submit" name="accountNumber" form="operationForm" value=<%=acc.getCardNumber()%>>
                        <%=locBundle.getString("acc_block_action")%></button> </td>
                </tr>
            <%;%>
        <%}%>
<%}%>
    </tbody>
</table>
