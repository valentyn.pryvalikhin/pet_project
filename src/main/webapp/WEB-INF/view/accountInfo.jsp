<%@ page import="java.util.ResourceBundle" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% ResourceBundle locBundle = (ResourceBundle) request.getAttribute("locBundle");%>

<html>
<body>
<form action="${pageContext.request.contextPath}/accountInfoServlet" id="accountData" method="post"></form>
<p><%=locBundle.getString("acc_card_number")%></p>
<p><c:out value="${accountForEdit.cardNumber}" /></p>
<p><%=locBundle.getString("acc_name")%></p>
<p><c:out value="${accountForEdit.name}" /></p>
<p><%=locBundle.getString("acc_amount")%></p>
<p><c:out value="${accountForEdit.amount}" /></p>

<c:choose>
    <c:when test="${accountForEdit.state == 0 && requestScope.fundsAreValid == null}">
        <p>
            <input type="hidden" name="action" form="accountData" value="add">
            <input type="hidden" name="cardNumber" form="accountData" value="${accountForEdit.cardNumber}">
            <input name="funds"  value="0.00" type="text" form="accountData">
            <button type="submit" form="accountData"><%=locBundle.getString("acc_add_funds_action")%></button>
        </p>

    </c:when>
    <c:when test="${accountForEdit.state == 0 && requestScope.fundsAreValid}">
        <p>
            <input type="hidden" name="action" form="accountData" value="add">
            <input type="hidden" name="cardNumber" form="accountData" value="${accountForEdit.cardNumber}">
            <input name="funds"  value="0.00" type="text" form="accountData">
            <button type="submit" form="accountData"><%=locBundle.getString("acc_add_funds_action")%></button>
        </p>
        <p style="color:green;font-size:75%"><%=locBundle.getString("acc_warning_success")%><%= request.getParameter("funds") %></p>
    </c:when>
    <c:when test="${accountForEdit.state == 0 && !requestScope.fundsAreValid}">
        <p>
            <input type="hidden" name="action" form="accountData" value="add">
            <input type="hidden" name="cardNumber" form="accountData" value="${accountForEdit.cardNumber}">
            <input name="funds"  value="<%= request.getParameter("funds") %>" type="text" form="accountData">
            <button type="submit" form="accountData"><%=locBundle.getString("acc_add_funds_action")%></button>
        </p>
        <p style="color:red;font-size:75%"><%=locBundle.getString("acc_warning_incorrect_input")%></p>
    </c:when>
    <c:otherwise>
        <p>&nbsp;</p>
    </c:otherwise>
</c:choose>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<form action="accountInfoServlet" id="accountState" method="post"></form>
<p>
<c:choose>
    <c:when test="${accountForEdit.state == 0}">
        <input type="hidden" name="action" form="accountState" value="block">
        <button type="submit" name="cardNumber" form="accountState" value="${accountForEdit.cardNumber}">
            <%=locBundle.getString("acc_block_action")%></button>
    </c:when>
    <c:when test="${accountForEdit.state == 1}">
        <input type="hidden" name="action" form="accountState" value="unblock">
        <button type="submit" name="cardNumber" form="accountState" value="${accountForEdit.cardNumber}">
            <%=locBundle.getString("acc_unblock_action")%></button>
    </c:when>
    <c:when test="${accountForEdit.state == 2}">
        <%=locBundle.getString("acc_status_unblocking")%>
    </c:when>
</c:choose>
</p>

<form action="accountInfoServlet" id="accountDelete" method="post"></form>
<p>
    <input type="hidden" name="action" form="accountDelete" value="delete">
    <button type="submit" name="cardNumber" form="accountDelete" value="${accountForEdit.cardNumber}"><%=locBundle.getString("acc_delete_action")%></button>
</p>


</body>
</html>